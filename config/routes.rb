MusicApp::Application.routes.draw do
  resources :users do
    collection do
      get 'activate'
    end
  end
  resource :session

  resources :bands do
    resources :albums, :only => [:new, :index, :create] do
      resources :tracks, :only => [:new, :index, :create]
    end
  end

  resources :albums, :only => [:show, :edit, :update, :destroy]
  resources :tracks, :only => [:show, :edit, :update, :destroy] do
    resources :notes
  end

end

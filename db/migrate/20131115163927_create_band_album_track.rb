class CreateBandAlbumTrack < ActiveRecord::Migration
  def change
    create_table :bands do |t|
      t.string :band_name, null: false
      t.timestamps
    end

    create_table :albums do |t|
      t.integer :band_id, null: false
      t.string :album_name, null: false
      t.timestamps
    end

    create_table :tracks do |t|
      t.integer :album_id, null: false
      t.string :track_name, null: false
      t.timestamps
    end

    add_index :bands, :band_name
    add_index :albums, :band_id
    add_index :tracks, :album_id
  end

end

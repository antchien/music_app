class SessionsController < ApplicationController

  def new
  end

  def create
    user = User.find_by_credentials(params[:user][:email], params[:user][:password])
    if login_user!(user)
      redirect_to current_user #supposed to go to User#show
    else
      redirect_to new_session_url #go back to sign-in page
    end
  end

  def destroy
    logout_user!
    redirect_to new_session_url
  end

end

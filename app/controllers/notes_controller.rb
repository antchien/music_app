class NotesController < ApplicationController

  def index
  end

  def show
    @note = Note.find(params[:id])
  end

  def new
  end

  def create
    @note = Note.new(params[:note])
    @note.user_id = current_user.id
    if @note.save
      flash[:notice] ||= []
      flash[:notice] << "New Note Created!"
      redirect_to @note.track
    else
      flash[:notice] ||= []
      flash[:notice] << "Unable to create new note"
      redirect_to track_notes_url(params[:track_id])
    end
  end

  def edit
  end

  def update
  end

  def destroy
    note = Note.find(params[:id])
    track = note.track
    Note.destroy(params[:id])
    redirect_to track
  end

end

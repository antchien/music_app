class AlbumsController < ApplicationController

  def index
  end

  def show
    @album = Album.find(params[:id])
  end

  def new
  end

  def create
    @album = Album.new(params[:album])
    if @album.save
      flash[:notice] ||= []
      flash[:notice] << "New Album Created!"
      redirect_to @album
    else
      flash[:notice] ||= []
      flash[:notice] << "Unable to create new album"
      redirect_to new_band_album_url
    end
  end

  def edit
  end

  def update
  end

  def destroy
    album = Album.find(params[:id])
    band = album.band
    Album.destroy(params[:id])
    redirect_to band
  end

end

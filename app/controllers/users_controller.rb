class UsersController < ApplicationController


  def new
  end

  def create
    @user = User.new(params[:user])
    @user.activated = false
    auth_token = SecureRandom::urlsafe_base64(16)
    @user.activation_token = auth_token
    if @user.save
      #login_user!(@user)
      #
      msg = UserMailer.welcome_email(@user, auth_token)
      msg.deliver!
      redirect_to bands_url
    else
      fail
      flash[:error] ||= []
      flash[:error] << @user.errors.full_messages
      redirect_to new_user_url #need to make sure this is correct
    end
  end

  def show
    redirect_to bands_url
  end

  def activate
    input_token = params[:activation_token]
    @user = User.find_by_activation_token(input_token)
    if @user
      if @user.activated == false
        @user.activated = true
        if @user.save
          login_user!(@user)
          redirect_to bands_url
        else
          flash[:error] ||= []
          flash[:error] << "Can not activate user"
        end
      else
        flash[:notice] ||= []
        flash[:notice] << "User already activated!"
        redirect_to new_session_url
      end
    else
      flash[:error] ||= []
      flash[:error] << "Can not find user!"
      redirect_to new_user_url
    end
  end
end

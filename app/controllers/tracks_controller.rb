class TracksController < ApplicationController

  def index
  end

  def show
    @track = Track.find(params[:id])
  end

  def new
  end

  def create
    @track = Track.new(params[:track])
    if @track.save
      flash[:notice] ||= []
      flash[:notice] << "New Track Created!"
      redirect_to @track
    else
      flash[:notice] ||= []
      flash[:notice] << "Unable to create new track"
      redirect_to new_band_album_track_url
    end
  end

  def edit
  end

  def update
  end

  def destroy
    track = Track.find(params[:id])
    album = track.album
    Track.destroy(params[:id])
    redirect_to album
  end

end

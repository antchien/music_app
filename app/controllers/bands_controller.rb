class BandsController < ApplicationController

  def index
  end

  def show
    #fail
    @band = Band.find(params[:id])
  end

  def new
  end

  def create
    @band = Band.new(params[:band])
    if @band.save
      flash[:notice] ||= []
      flash[:notice] << "New Band Created!"
      redirect_to @band
    else
      flash[:notice] ||= []
      flash[:notice] << "Unable to create new band"
      redirect_to new_band_url
    end
  end

  def edit
  end

  def update
  end

  def destroy
    Band.destroy(params[:id])
    redirect_to bands_url
  end

end

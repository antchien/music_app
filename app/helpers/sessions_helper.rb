module SessionsHelper

  def current_user
    @current_user ||= User.find_by_session_token(session[:session_token])
  end

  def logged_in?
    !current_user.nil?
  end

  def logout_user!
    current_user.session_token = nil
    if current_user.save
      session[:session_token] = nil
    else
      flash[:error] ||= []
      flash[:error] << "Can not reset session_token to log in user"
    end
  end

  def login_user!(user)
    user.reset_session_token!
    if user.save && user.activated
      session[:session_token] = user.session_token
      true
    else
      flash[:error] ||= []
      flash[:error] << "User is not activated" if !user.activated
      flash[:error] << "Can not reset session_token to log in user" if !user.save
      false
    end
  end
end

class User < ActiveRecord::Base

  attr_accessible :email, :password_digest, :session_token, :activation_token, :password

  validates :email, :password_digest, :session_token, presence: true
  validates :email, uniqueness: true

  before_validation :ensure_session_token

  has_many(
  :notes,
  class_name: "Note",
  foreign_key: :user_id,
  primary_key: :id,
  dependent: :destroy
  )

  has_many :noted_tracks, :through => :notes, source: :track

  def self.generate_session_token
    SecureRandom::urlsafe_base64(16)
  end

  def reset_session_token!
    self.session_token = self.class.generate_session_token
    #may need to set sessions[:session_token] to @session_token
  end

  def password=(secret)
    self.password_digest = BCrypt::Password.create(secret)
  end

  def is_password?(secret)
    BCrypt::Password.new(self.password_digest).is_password?(secret)
  end

  def self.find_by_credentials(email, secret)
    user = User.find_by_email(email)
    if user.nil?
      flash[:error] ||= []
      flash[:error] << "No user found with this email address"
      return nil
    else
      return user if user.is_password?(secret)
      # flash[:error] ||= []
#       flash[:error] << "Incorrect password for this user!"
      return nil
    end
  end

  def ensure_session_token
    self.session_token ||= self.class.generate_session_token
  end

end
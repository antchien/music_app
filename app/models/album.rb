class Album < ActiveRecord::Base
  attr_accessible :album_name, :band_id

  belongs_to(
  :band,
  class_name: "Band",
  foreign_key: :band_id,
  primary_key: :id
  )

  has_many(
  :tracks,
  class_name: "Track",
  foreign_key: :album_id,
  primary_key: :id,
  dependent: :destroy
  )

  def self.fetch_by_band_id(band_id)
    Band.find(band_id).albums
  end

end
class UserMailer < ActionMailer::Base
  default from: "notification@example.com"

  def welcome_email(user, auth_code)
    @user = user
    @url = 'http://localhost:3000/users/activate?activation_token=' + auth_code
    mail(to: user.email, subject: 'Welcome to Music App!')
  end

end
